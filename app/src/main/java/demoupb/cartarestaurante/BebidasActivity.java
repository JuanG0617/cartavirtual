package demoupb.cartarestaurante;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class BebidasActivity extends Activity {

    private String[] bebida = {"Jugo de naranja","Jugo de mora","Jugo de mandarina","Jugo de mango","Limonada natural","Limonada cerezada","Limonada de coco","Limonada de hierbabuena","Gaseosas","Agua","Té helado"};
    private String[] precios = {"$5,900","$5,900","$7,500","$5,900","$5,900","$6,900","$6,900","$6,900","$4,500","$3,900","$4,500"};
    private TextView tv1;
    private ListView lv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bebidas);

        tv1 = (TextView)findViewById(R.id.tv1);
        lv1 = (ListView)findViewById(R.id.list1);
        ArrayAdapter<String>adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, bebida);
        lv1.setAdapter(adapter);

        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                tv1.setText("El precio de "+ lv1.getItemAtPosition(i) + " es: "+ precios[i]);
            }
        });
    }
}
