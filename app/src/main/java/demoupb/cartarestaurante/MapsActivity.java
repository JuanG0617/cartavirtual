package demoupb.cartarestaurante;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        float zoomLevel = 16.0f;

        //Ubicacion del restaurante 2
        LatLng Restaurante2 = new LatLng(6.241743, -75.589577);
        mMap.addMarker(new MarkerOptions().position(Restaurante2).title("Ubicación restaurante sede Boulevard"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Restaurante2));

        //Ubicacion del restaurante 3
        LatLng Restaurante3 = new LatLng(6.243462, -75.590400);
        mMap.addMarker(new MarkerOptions().position(Restaurante3).title("Ubicación restaurante sede bloque Derecho"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Restaurante3));

        //Ubicacion del restaurante 4
        LatLng Restaurante4 = new LatLng(6.261360, -75.593548);
        mMap.addMarker(new MarkerOptions().position(Restaurante4).title("Ubicación restaurante sede Estadio"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Restaurante4));

        //Ubicacion del restaurante principal
        LatLng Restaurante = new LatLng(6.240725, -75.589173);
        mMap.addMarker(new MarkerOptions().position(Restaurante).title("Ubicación restaurante principal"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(Restaurante));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Restaurante, zoomLevel));
    }


}
