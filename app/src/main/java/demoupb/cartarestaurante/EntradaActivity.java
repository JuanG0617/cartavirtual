package demoupb.cartarestaurante;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class EntradaActivity extends Activity {

    private String[] entrada = {"Arepas Trío","Aros de cebolla","Canasticas","Empanadas", "Papas a la francesa","Patacones con hogao y guacamole","Sopa de Ahuyama","Sopa de Tomate","Sopa de Verduras"};
    private String[] precios = {"$17,500","$6,900","$14,900","$8,900","$6,500","$10,500","$8,500","$8,500","$8,500"};
    private TextView tv2;
    private ListView lv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrada);

        tv2 = (TextView)findViewById(R.id.tv2);
        lv2 = (ListView)findViewById(R.id.list2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, entrada);
        lv2.setAdapter(adapter);

        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                tv2.setText("El precio de "+ lv2.getItemAtPosition(i) + " es: "+ precios[i]);
            }
        });
    }
}
