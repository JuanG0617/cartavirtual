package demoupb.cartarestaurante;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class CartaActivity extends Activity {

    String[] productos = new String[]{"Entradas", "Platos Fuertes", "Bebidas", "Postres"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carta);

        ListView lv = findViewById(android.R.id.list);
        ListAdapter la = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, productos);
        lv.setAdapter(la);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0){ //Entradas
                    Intent intencion = new Intent(view.getContext(), EntradaActivity.class);
                    startActivity(intencion);
                }else if(position == 1){ //Platos fuertes
                    Intent intencion = new Intent(view.getContext(), PlatoActivity.class);
                    startActivity(intencion);
                }else if(position == 2){ //Bebidad
                    Intent intencion = new Intent(view.getContext(), BebidasActivity.class);
                    startActivity(intencion);
                }else if(position == 3) { //Postres
                    Intent intencion = new Intent(view.getContext(), PostresActivity.class);
                    startActivity(intencion);
                }
            }
        });



    }




}
