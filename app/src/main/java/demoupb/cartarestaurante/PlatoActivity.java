package demoupb.cartarestaurante;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PlatoActivity extends Activity {

    private String[] fuerte =  {"Fettuccine Alfredo con Pollo y Brócoli","Fettuccine Alfredo con Salmón","Lasagna Bolognesa","Lasagna de Pollo con Champiñones","Spaguetti a la Bolognesa","Churrasco con papas a la francesa","Punta de Anca con papas a la francesa","Arepa de Cañon con Hogao","Arepa de Cañon con camarones al Ajillo","Arepa de Cañon con Salsa de Frutas","Hamburguesa doble","Hamburguesa sencilla","Ensalada César","Ensalada del Chef"};
    private String[] precios = {"$24,900","$28,900","$20,900","$22,900","$19,900","$23,900","$24,900","$19,900","$23,100","$20,700","$20,500","$14,900","$24,500","$22,900"};
    private TextView tv3;
    private ListView lv3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plato);

        tv3 = (TextView)findViewById(R.id.tv3);
        lv3 = (ListView)findViewById(R.id.list3);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, fuerte);
        lv3.setAdapter(adapter);

        lv3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                tv3.setText("El precio de "+ lv3.getItemAtPosition(i) + " es: "+ precios[i]);
            }
        });
    }
}
