package demoupb.cartarestaurante;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.Toast;

public class CalificarActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calificar);

        Switch comentario = (Switch)findViewById(R.id.switch1);
        comentario.setChecked(true);
        comentario.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                EditText nombre = (EditText)findViewById(R.id.editText);
                EditText telefono = (EditText)findViewById(R.id.editText2);
                EditText comentar = (EditText)findViewById(R.id.editText3);
                if (buttonView.isChecked()){
                    nombre.setVisibility(View.VISIBLE);
                    telefono.setVisibility(View.VISIBLE);
                    comentar.setVisibility(View.VISIBLE);
                }
                else{
                    nombre.setVisibility(View.INVISIBLE);
                    telefono.setVisibility(View.INVISIBLE);
                    comentar.setVisibility(View.INVISIBLE);
                }
            }
        });



    }

    void borrarComentario(View view){

        EditText nombre = (EditText)findViewById(R.id.editText);
        EditText telefono = (EditText)findViewById(R.id.editText2);
        EditText comentar = (EditText)findViewById(R.id.editText3);
        RatingBar estrellas = (RatingBar)findViewById(R.id.ratingBar);

        nombre.setText("");
        telefono.setText("");
        comentar.setText("");
        estrellas.setRating(0);
        Toast.makeText(getApplicationContext(), "¡Gracias por su comentario!" , Toast.LENGTH_LONG).show();

    }
}
