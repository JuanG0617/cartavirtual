package demoupb.cartarestaurante;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PostresActivity extends Activity {

    private String[] postres = {"Brownie al horno con helado","Galleta al horno con helado","Waffle con helado","Tiramisú","Postre de tres leches","Flan de caramelo"};
    private String[] precios = {"$12,500","$12,500","$10,900","$14,200","$9,900","$8,900"};
    private TextView tv4;
    private ListView lv4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postres);

        tv4 = (TextView)findViewById(R.id.tv4);
        lv4 = (ListView)findViewById(R.id.list4);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, postres);
        lv4.setAdapter(adapter);

        lv4.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                tv4.setText("El precio de "+ lv4.getItemAtPosition(i) + " es: "+ precios[i]);
            }
        });
    }
}
